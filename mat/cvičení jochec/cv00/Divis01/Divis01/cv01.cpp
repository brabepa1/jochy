// trida clovek
//jmeno vek rodne cislo

//trida objekt
//ciste virtualni metoda vypis()

#include <string>
#include <iostream>


class Object{

public:
	virtual void vypis() = 0;
};

class Clovek : public Object{

public:

	Clovek(std::string jm, unsigned int v, unsigned long rc) :
		jmeno(jm), vek(v), rodneCislo(rc){

	}

	virtual void vypis();

private:

	std::string jmeno;
	unsigned int vek;
	unsigned long rodneCislo;

};

void Clovek::vypis(){

	std::cout << jmeno << " " << vek << " " << rodneCislo << std::endl;
}

int main(){

	Clovek prvni("Roman", 123, 123354648);
	prvni.vypis();

	return 0;
}