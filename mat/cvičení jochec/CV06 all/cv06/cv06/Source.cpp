#include <iostream>
#include "Header.h"
using namespace std;

int main(){

	Par<int, int> sablonaInt(123,456);
	cout << sablonaInt.VratKlic() << " " << sablonaInt.VratHodnotu() << endl;

	Par<int*, int*> sablonaUkInt(new int(123),new int(456));
	cout << sablonaUkInt.VratKlic() << " " << sablonaUkInt.VratHodnotu() << endl;

	/*Sablona<double> sablonaDouble(123.123);
	cout << sablonaDouble.Dej() << endl;

	Sablona<int*> sablonaUkInt(new int(123));
	cout << sablonaInt.Dej() << endl;

	Sablona<double*> sablonaUkDouble(new double(123.123));
	cout << sablonaDouble.Dej() << endl;

	Sablona<char*> sablonaUkChar("hello world");
	cout << sablonaUkChar.Dej() << endl;*/


	system("pause");
	return 0;

}