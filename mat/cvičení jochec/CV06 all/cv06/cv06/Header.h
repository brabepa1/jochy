#include <iostream>

template <typename Klic, typename Hodnota>
class Par
{

private:

	Klic pKlic;
	Hodnota pHodnota;

public:
	Par(Klic klic, Hodnota hodnota);
	~Par(void);

	Klic& VratKlic();
	Hodnota& VratHodnotu();
	bool operator==(const Par<Klic, Hodnota>& druhy);
	bool operator!=(const Par<Klic, Hodnota>& druhy);

};

template <typename Klic, typename Hodnota>
class Par<Klic*, Hodnota*>
{

private:

	Klic* pKlic;
	Hodnota* pHodnota;

public:
	Par(Klic* klic, Hodnota* hodnota);
	~Par(void);

	Klic& VratKlic();
	Hodnota& VratHodnotu();
	bool operator==(const Par<Klic*, Hodnota>& druhy);
	bool operator!=(const Par<Klic*, Hodnota>& druhy);

};

template <>
class Par<char*, char*>
{

private:

	char* pKlic;
	char* pHodnota;

public:
	Par(char* klic, char* hodnota);
	~Par(void);

	char*& VratKlic();
	char*& VratHodnotu();
	bool operator==(const Par<char*, char*>& druhy);
	bool operator!=(const Par<char*, char*>& druhy);

};

template <typename Klic, typename Hodnota>
Par<Klic, Hodnota>::Par(Klic klic, Hodnota hodnota){

	pKlic = klic;
	pHodnota = hodnota;

}

template <typename Klic, typename Hodnota>
Par<Klic, Hodnota>::~Par(void){

	//delete pKlic;
	//delete pHodnota;

}

template <typename Klic, typename Hodnota>
Klic& Par<Klic, Hodnota>::VratKlic(){

	return pKlic;
}

template <typename Klic, typename Hodnota>
Hodnota& Par<Klic, Hodnota>::VratHodnotu(){

	return pHodnota;
} 


/////////////parcialni

template <typename Klic, typename Hodnota>
Par<Klic*, Hodnota*>::Par(Klic* klic, Hodnota* hodnota){

	pKlic = klic;
	pHodnota = hodnota;

}

template <typename Klic, typename Hodnota>
Par<Klic*, Hodnota*>::~Par(void){

	delete pKlic;
	delete pHodnota;

}

template <typename Klic, typename Hodnota>
Klic& Par<Klic*, Hodnota*>::VratKlic(){

	return *pKlic;
}

template <typename Klic, typename Hodnota>
Hodnota& Par<Klic*, Hodnota*>::VratHodnotu(){

	return *pHodnota;
}






