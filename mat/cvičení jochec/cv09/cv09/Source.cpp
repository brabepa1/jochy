#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <iostream>
#include <deque>
#include <functional>
#include <string>

using namespace std;

int funkce() {

	static int i = 0;
	return i++;
}

struct funkcniObjekt {

	funkcniObjekt(int i) : i(i) {}

	int operator()() {

		return i++;
	}

private:

	int i;

};

struct Osoba{

	string _jmeno;
	string _prijmeni;
	unsigned long _rodneCislo;
	unsigned int _psc;

	Osoba(string jmeno, string prijmeni, unsigned long rodneCislo, unsigned int psc){
	
		_jmeno = jmeno;
		_prijmeni = prijmeni;
		_rodneCislo = rodneCislo;
		_psc = psc;
	
	};

	void vypis(){

		cout << "Jmeno: " << _jmeno << "Prijmeni: " << _prijmeni << "rodne cislo: " << _rodneCislo << "psc: " << _psc << endl;
	}

};

int main() {

	Osoba o1("Roman", "Jochec", 1616156156, 16166);
	Osoba o2("Ondra", "Zeman", 54646564, 45666);
	Osoba o3("Jakub", "Spitzer", 787151, 111111);

	vector<Osoba> osoby;

	osoby.push_back(o1);
	osoby.push_back(o2);
	osoby.push_back(o3);


	for_each(osoby.begin(), osoby.end(), mem_fun_ref(&Osoba::vypis));




	vector<int> vect;

	//vect.resize(10);
	//generate_n(vect.begin(), 10, funkce);

	generate_n(back_inserter(vect), 10, funkce);
	copy(vect.begin(), vect.end(), ostream_iterator<int>(cout, " "));
	cout << endl;

	generate(vect.begin(), vect.end(), funkcniObjekt(20));
	copy(vect.begin(), vect.end(), ostream_iterator<int>(cout, " "));
	cout << endl;

	int i = 0;
	generate(vect.begin(), vect.end(), [&i]() {return i++; });
	copy(vect.begin(), vect.end(), ostream_iterator<int>(cout, " "));
	cout << endl;

	deque<int> dequ;
	transform(
		vect.begin(),
		vect.end(),
		front_inserter(dequ),
		[](int i) {return -i; }
	);
	copy(dequ.begin(), dequ.end(), ostream_iterator<int>(cout, " "));
	cout << endl;

	transform(
		vect.begin(),
		vect.end(),
		dequ.begin(),
		ostream_iterator<int>(cout, " "),
		multiplies<int>()
		);
	cout << endl;

	transform(
		vect.begin(),
		vect.end(),
		ostream_iterator<int>(cout, " "),
		bind2nd(multiplies<int>(), 3)
		);
	cout << endl;

	transform(
		vect.begin(),
		vect.end(),
		ostream_iterator<int>(cout, " "),
		[](int i) {return i * 3; }
	);
	cout << endl;

	system("pause");
	return 0;
}