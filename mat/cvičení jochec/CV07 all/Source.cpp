#include <string>
#include <iostream>
using namespace std;

/*
Ukol c.1
Vytvorte program, ktery z argumentu prikazoveho radku generuje jmena docasnych souboru s koncovkou tmp. Napriklad
pokud spustime program s nasledujicim prikazem:

PraceSeStringem soubor1.exe adresar1 soubor2.jpg adresar2
Vytup bude
soubor1.exe -> soubor1.tmp
adresar1 -> adresar1.tmp
soubor2.jpg -> soubor2.tmp
....

*/
#include <vector>
void PraceSeStringem()
{
	string s;
	//cin >> s;
	getline(cin, s); // musime pouzit getline misto cin kdyz chcem pracovat s mezerama
	size_t space = s.find(' '); //prvni mezera
	std::vector<size_t> vectorOfSpaces;

	while (space != string::npos)
	{
		vectorOfSpaces.push_back(space);
		space = s.find(" ", space + 1); //posunem se o jeden char dale
	}
	for (int i = 0; i < vectorOfSpaces.size(); i++){
		std::cout << vectorOfSpaces[i] << " ";
	}

	size_t tecka = s.find('.');
	while (tecka != string::npos)
	{
		if (tecka != string::npos)
		{
			//s.erase(s.find('.'), )
		}
		tecka = s.find('.', tecka + 1); //posunem se o jeden char dale

	}

	//if (s.find('.') != string::npos){
	//	s.erase(s.find('.'), string::npos); //smaze to vsechno od tecky do konce
	//}
	//s.append(".tmp");
	//cout << s;

}

// Ukol c.2
// Hello world example -> olleh dlrow elpmaxe
void VypisSlovaVeVetePoZpatku()
{
	string s;
	getline(cin, s);
	string newS;
	//for (int i = s.size() - 1; i >= 0; i--)
	//{
	//	newS.push_back(s[i]);
	//}

	string sub;
	size_t space = s.find(' '); //prvni mezera
	std::vector<size_t> vectorOfSpaces;
	while (space != string::npos)
	{
		vectorOfSpaces.push_back(space);
	/*	sub = s.substr(space, s.find(" ", space + 1));
		for (int i = sub.size() - 1; i >= 0; i--)
		{
			newS.push_back(sub[i]);
		}*/
		space = s.find(" ", space + 1); //posunem se o jeden char dale
	}
	for (size_t i = 0; i < vectorOfSpaces.size()-1; i++)
	{
		sub = s.substr(vectorOfSpaces[i], vectorOfSpaces[i+1]);
		 
		/*for (int i = sub.size() - 1; i >= 0; i--)
		{
			newS.push_back(sub[i]);
		}*/
	}
	 cout << newS;
}


void testik(){
	string sen;
	string pozadu;
	getline(cin, sen);

	std::vector<std::string> words;

	//find first space
	size_t start = 0, end = sen.find(' ');
	//as long as there are spaces
	while (end != std::string::npos)
	{
		//get word 
		words.push_back(sen.substr(start, end - start));

		//search next space (of course only after already found space)
		start = end + 1;
		end = sen.find(' ', start);
	}

	//last word
	words.push_back(sen.substr(start));

	for (size_t i = 0; i < words.size(); i++)
	{
		for (int j = words[i].size() - 1; j >= 0; j--)
		{
			pozadu.push_back(words[i].at(j));
		}
		pozadu.push_back(' ');
	}
	std::cout << "CELKEM " << pozadu;
}

int main(){
	string s;
	string s2("hello");
	string s3(s2);
	string s4(s2, 2);

	cout << s2 << endl;

	//cin >> s;
	//getline(cin, s);
	//cout << s;

	s += (s2 + s3) + " world";
	s.append("!!!");
	s.push_back('?');
	cout << s;

	s.insert(0, "prvni");
	//s.replace();
	//s.erase();

	cout << s.size() << endl; // nad jakoukoliv veci kdyz das F1 tak se ti otevre cpp.com a informace o tom!!!
	cout << (s2 == s3 ? "stejne" : "ruzne") << endl;

	//s.find
	const char* c = s.c_str(); //prevod na ceckovskej string(char*)

	s.at(0);
	s[0];


	//	PraceSeStringem();
	//VypisSlovaVeVetePoZpatku();
	testik();
	return 0;
}