#include "Tabulka.h"
#include <cstdlib>

int Tabulka::pocetInstanci = 0;

void Tabulka::alokace()
{
	pole = new int*[_pRadku];
	for (int i = 0; i < _pRadku; i++)
	{
		pole[i] = new int[_pSloupcu];
	}
}

void Tabulka::dealokace()
{
	for (int i = 0; i < _pRadku; i++)
	{
		delete[] pole[i];
	}

	delete[] pole;
}

void Tabulka::naplnPole()
{
	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			pole[i][j] = -10 + rand() % 50;
		}
	}
}

Tabulka::Tabulka()
: _pRadku(3), _pSloupcu(3)
{
	Tabulka::pocetInstanci++;
	alokace();
	naplnPole();
}

Tabulka::Tabulka(rozmer_t r, rozmer_t s)
: _pRadku(r), _pSloupcu(s)
{
	Tabulka::pocetInstanci++;
	alokace();
	naplnPole();
}

Tabulka::~Tabulka()
{
	Tabulka::pocetInstanci--;
	dealokace();
}

int Tabulka::SectiVsechnyHodnoty()
{
	int suma = 0;

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			suma += pole[i][j];
		}
	}

	return suma;
}

int Tabulka::NajdiMaximalniHodnotu()
{
	int max = pole[0][0];

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			if (max < pole[i][j]) max = pole[i][j];
		}
	}

	return max;
}

int Tabulka::NajdiMinimalniHodnotu()
{
	int min = pole[0][0];

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			if (min > pole[i][j]) min = pole[i][j];
		}
	}

	return min;
}

void Tabulka::ProvedFci(f fce)
{
	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			fce(pole[i][j]);
		}
	}
}


Tabulka::Tabulka(const Tabulka& sec)
{
	_pSloupcu = sec._pSloupcu; 
	_pRadku = sec._pRadku; 

	alokace(); 

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			pole[i][j] = sec.pole[i][j]; 
		}
	}
}

Tabulka& Tabulka::operator=(const Tabulka& sec)
{
	Tabulka& tab = *this;

	tab.dealokace();

	tab._pRadku = sec._pRadku;
	tab._pSloupcu = sec._pSloupcu;

	tab.alokace();

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++)
		{
			tab.pole[i][j] = sec.pole[i][j]; 
		}
	}

	return tab;
}

ostream& operator<<(ostream& os, const Tabulka& sec)
{
	os << "sloupcu: " << sec._pSloupcu << endl;
	os << "radku: " << sec._pRadku << endl;

	for (int i = 0; i < sec._pRadku; i++)
	{
		for (int j = 0; j < sec._pSloupcu; j++)
		{
			os << sec.pole[i][j] << ", ";
		}
		os << endl;
	}

	return os;
}

Tabulka& Tabulka::operator+(const Tabulka& sec)
{
	Tabulka& t = *this;

	if (_pRadku == sec._pRadku && _pSloupcu == sec._pSloupcu)
	{
		for (int i = 0; i < _pRadku; i++)
		{
			for (int j = 0; j < _pSloupcu; j++){
				t.pole[i][j] += sec.pole[i][j];
			}
		}
	}

	return t;
}

Tabulka& Tabulka::operator-(const Tabulka& sec)
{
	Tabulka& t = *this;

	if (_pRadku == sec._pRadku && _pSloupcu == sec._pSloupcu)
	{
		for (int i = 0; i < _pRadku; i++)
		{
			for (int j = 0; j < _pSloupcu; j++){
				t.pole[i][j] -= sec.pole[i][j];
			}
		}
	}

	return t;
}

Tabulka& Tabulka::operator*(int cislo)
{
	Tabulka& t = *this;

	for (int i = 0; i < _pRadku; i++)
	{
		for (int j = 0; j < _pSloupcu; j++){
			t.pole[i][j] *= cislo;
		}
	}

	return t;
}

Tabulka& Tabulka::operator/(int cislo)
{
	Tabulka& t = *this;

	if (cislo != 0)
	{
		for (int i = 0; i < _pRadku; i++)
		{
			for (int j = 0; j < _pSloupcu; j++)
			{
				t.pole[i][j] /= cislo;
			}
		}
	}
	return t;
}