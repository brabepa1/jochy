#include "Tabulka.h"
#include <iostream>
#include <ctime>

using namespace std;

int main(void)
{
	srand(time(NULL));

	cout << "pocetInstanci: " << Tabulka::getPocetInstanci() << endl;

	cout << endl;


	Tabulka tabulka_1 = Tabulka(3, 3);
	Tabulka tabulka_2 = Tabulka();

	cout << "pocetInstanci: " << Tabulka::getPocetInstanci() << endl;

	cout << "tabulka_1" << endl;
	cout << tabulka_1 << endl;

	cout << "tabulka_2" << endl;
	cout << tabulka_2 << endl;

	tabulka_1 + tabulka_2;

	cout << "tabulka_1 + tabulka_2" << endl;
	cout << tabulka_1 << endl;

	tabulka_2*-1;
	cout << "tabulka_2 * -1" << endl;
	cout << tabulka_2 << endl;	

	tabulka_2*5;
	cout << "tabulka_2 *5" << endl;
	cout << tabulka_2 << endl;

	tabulka_1 / 5;
	cout << "tabulka_1 /5" << endl;
	cout << tabulka_1 << endl;

	return 0;
}