#ifndef _TABULKA_H_
#define _TABULKA_H_

#include <iostream>

using namespace std;

class Tabulka
{
public:
	typedef unsigned int rozmer_t;

	Tabulka(rozmer_t r, rozmer_t s);
	Tabulka();
	~Tabulka();

	int SectiVsechnyHodnoty();
	int NajdiMaximalniHodnotu();
	int NajdiMinimalniHodnotu();

	typedef void(*f)(int&);
	void ProvedFci(f);

	void ProhoditDvaPrvkyTabulky(int s1, int r1, int s2, int r2);

	Tabulka(const Tabulka& second);
	Tabulka& operator=(const Tabulka& second);
	friend ostream& operator<<(ostream& os, const Tabulka& sec);

	Tabulka& operator+(const Tabulka& sec);
	Tabulka& operator-(const Tabulka& sec);
	Tabulka& operator*(int i);
	Tabulka& operator/(int i);

	static int getPocetInstanci()
	{
		return pocetInstanci;
	}

private:
	rozmer_t _pRadku;
	rozmer_t _pSloupcu;

	int** pole;

	void alokace();
	void naplnPole();
	void dealokace();

	static int pocetInstanci;
};

#endif