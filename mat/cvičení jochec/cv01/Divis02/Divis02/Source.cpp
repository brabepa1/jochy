#include <iostream>

//prohodi dve promene pomoci ukazatelu
void prohod(int* pa, int* pb){

	int pom = *pa;
	*pa = *pb;
	*pb = pom;
}



void prohodReferenci(int& a, int& b){ //int& a datovy typ reference

	int pom = a;
	a = b;
	b = pom;
}

int main(){

	int a = 25;
	int b = 100;

	std::cout << "a = " << a << " b = " << b << std::endl;
	prohod(&a, &b);
	std::cout << "a = " << a << " b = " << b << std::endl;

	std::cout << "a = " << a << " b = " << b << std::endl;
	prohodReferenci(a,b);
	std::cout << "a = " << a << " b = " << b << std::endl;

	return 0;

}