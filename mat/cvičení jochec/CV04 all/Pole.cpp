#include <stdexcept>
#include <string>
#include <iostream>

class Pole {
public:
	Pole(size_t rozmer) {
		if (rozmer == 0) {
			throw std::invalid_argument("...");
		}

		this->rozmer = rozmer;
		pole = new int[rozmer];

		for (size_t i = 0; i < rozmer; i++) {
			pole[i] = 0;
		}
	}

	Pole(std::string str) {
		// std = "110123";
		// pole = {1, 1, 0, 1, 2, 3 }
		rozmer = str.size();
		pole = new int[rozmer];

		for (size_t i = 0; i < rozmer; i++) {
			if (str[i] < '0' || str[i] > '9') {
				delete[] pole;
				throw std::domain_error("...");
			}

			//char c = str[i];
			//char cc = str.at(i);

			int ii = str[i] - '0';
			//int ix = atoi(str.substr(i, 1).c_str());

			pole[i] = ii;
		}
	}

	~Pole() {
		delete[] pole;
	}

	int& operator[](size_t i) {
		if (i >= rozmer) {
			throw std::out_of_range("i je mimo rozsah");
		}
		return pole[i];
	}

	const int& operator[](size_t i) const {
		if (i >= rozmer) {
			throw std::out_of_range("i je mimo rozsah");
		}
		return pole[i];
	}

private:
	int* pole;
	size_t rozmer;

};

int main() {
	Pole p(std::string("0123456789"));

	p[5] = 123;
	std::cout << p[5] << std::endl;

	return 0;
}
