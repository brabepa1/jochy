#include <iostream>
#include <string>
using namespace std;

namespace Baze{

	class Objekt{

		private:
			
			unsigned short invCislo;

	public:

		unsigned short VratInventarniCislo() const{

			return invCislo;
		}

		virtual void VypisInformace() const = 0;

		friend bool operator>(const Objekt& levy, const Objekt& pravy){

			return levy.VratInventarniCislo() > pravy.VratInventarniCislo();

		}
	};

	void Objekt::VypisInformace() const{

		cout << "Inventarni cislo: " << Baze::Objekt::VratInventarniCislo << endl;
	}


}

namespace Objekty{

	namespace Nabytek{

		class Stul : public Baze::Objekt{

			string barva;
			string material;

			Stul(string Barva, string Material){

				barva = Barva;
				material = Material;
			}

			void VypisInformace()const override{
			
				Baze::Objekt::VypisInformace();
				cout << "Barva: " << barva << endl;
				cout << "Material: " << material << endl;

			}

		};


		class Zidle : public Baze::Objekt{

			bool otocna;
			bool ergonomicka;

			Zidle(bool Otocna, bool Ergonomicka){

				otocna = Otocna;
				ergonomicka = Ergonomicka;
			}

			void VypisInformace()const{

				Baze::Objekt::VypisInformace();
				cout << "Otocna: " << otocna << endl;
				cout << "Ergonomicka: " << ergonomicka << endl;

			}

		};


		class Skrin : public Baze::Objekt{

			string material;
			unsigned int rozmerVyska;
			unsigned int rozmerSirka;
			unsigned int rozmerHloubka;

			Skrin(string Material, unsigned int RozmerVyska, unsigned int RozmerSirka, unsigned int RozmerHloubka){

				material = Material;
				rozmerVyska = RozmerVyska;
				rozmerSirka = RozmerSirka;
				rozmerHloubka = RozmerHloubka;
			}

			void VypisInformace()const{

				Baze::Objekt::VypisInformace();
				cout << "Material: " << material << endl;
				cout << "RozmerVyska: " << rozmerVyska << endl;
				cout << "RozmerSirka: " << rozmerSirka << endl;
				cout << "RozmerHloubka: " << rozmerHloubka << endl;

			}
		};
	}


	class Inventar{

	private:
		Baze::Objekt** poleInventar;
		int maxPocet;
		int pocitadlo = 0;

	public:

		Inventar(size_t pocet){	
			
			maxPocet = pocet;
			poleInventar = new Baze::Objekt*[pocet];

			for (int i = 0; i < pocet; i++)
			{
				poleInventar[i] = nullptr;
			}			

		}

		void Pridej(Baze::Objekt* const objekt){

			if (pocitadlo < maxPocet)
			{
				poleInventar[pocitadlo] = objekt;
				pocitadlo++;
			}
			

		}


	};

}

int main(){



	system("pause");
	return 0;
}