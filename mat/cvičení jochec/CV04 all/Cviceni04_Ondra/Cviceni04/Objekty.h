#pragma once

#include "Objekt.h"
#include <string>
#include <stdexcept>

namespace Objekty {

	namespace Nabytek {

		class Stul: public Baze::Objekt{
		private: 
			string _material;
			string _barva;
		public:

			Stul(string material, string barva) : _barva(barva), _material(material){
				Baze::Objekt::nastavInventarniCislo(rand()/100);
			}

			void vypisInformace() const override {
				Baze::Objekt::vypisInformace();
				cout << "Stul" << endl;
				cout << "Material: " << _material << endl;
				cout << "Barva: " << _barva << endl;
			}
		};

		class Zidle : public Baze::Objekt {
		private:
			bool _otocna;
			bool _ergonomicka;
		public:

			Zidle(bool ergonomicka, bool otocna) : _otocna(otocna), _ergonomicka(ergonomicka) {
				Baze::Objekt::nastavInventarniCislo(rand()/100);
			}

			void vypisInformace() const override {
				Baze::Objekt::vypisInformace();
				cout << "Zidle" << endl;
				cout << "Ergonomicka: " << _ergonomicka << endl;
				cout << "Otocna: " << _otocna << endl;
			}
		};

		class Skrin : public Baze::Objekt {
		private:
			string _material;
			unsigned int _vyska;
			unsigned int _sirka;
			unsigned int _hloubka;
		public:

			Skrin(string material, unsigned int vyska, unsigned int hloubka, unsigned int sirka) 
				: _vyska(vyska), _material(material), _sirka(sirka), _hloubka(hloubka) {
				Baze::Objekt::nastavInventarniCislo(rand()/100);
			}

			void vypisInformace() const override {
				Baze::Objekt::vypisInformace();
				cout << "Skrin" << endl;
				cout << "Material: " << _material << endl;
				cout << "Vyska: " << _vyska << endl;
				cout << "Sirka: " << _sirka << endl;
				cout << "Hloubka: " << _hloubka << endl;
			}
		};
	}

	class Inventar {
	private:
		Baze::Objekt** _pole;
		int _rozmer;
		int _polozka = 0;
	public:
		Inventar(size_t pocet) {
			_pole = new Baze::Objekt*[pocet];
			_rozmer = pocet;
		}

		void Pridej(Baze::Objekt* const objekt) {
			if (_polozka >= _rozmer) {
				throw out_of_range("Nelze vlo�it dal�� prvek!");
			}

			_pole[_polozka] = objekt;
			_polozka++;
		}

		unsigned long NajdiLimit() {
			unsigned long _nejvetsiInventarni = _pole[0]->vratInventarniCislo();
			for (size_t i = 0; i < _polozka - 1; i++)
			{
				if (_polozka == 0) {
					throw out_of_range("Pole je pr�zdn�");
				}
				if (_pole[i] > _pole[i + 1]) {
					_nejvetsiInventarni = _pole[i+1]->vratInventarniCislo();
				}
			}
			return _nejvetsiInventarni;
		}

		void VypisInventar() const {
			for (size_t i = 0; i < _rozmer; i++)
			{
				if (_rozmer == 0) {
					throw out_of_range("Pole je pr�zdn�");
				}

				_pole[i]->vypisInformace();
			}
		}

	};
}