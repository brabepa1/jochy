#pragma once

#include <iostream>

using namespace std;

namespace Baze {

	class Objekt {

	private:
		unsigned short _inventarniCislo = 0;
	public:

		unsigned short vratInventarniCislo() const {
			return _inventarniCislo;
		}

		virtual void vypisInformace() const = 0;

		friend bool operator>(const Objekt& left, const Objekt& right) {
			return left.vratInventarniCislo() > right.vratInventarniCislo();
		}

		void nastavInventarniCislo(int cislo) {
			_inventarniCislo = cislo;
		}
	};

	void Objekt::vypisInformace() const {
		cout << "Inventarni cislo: " << Objekt::vratInventarniCislo() << endl;
	}
}