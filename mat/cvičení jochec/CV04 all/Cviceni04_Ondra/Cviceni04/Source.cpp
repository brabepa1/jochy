#include "Objekty.h"
#include <time.h>

void main() {
	
	srand(time(NULL));

	Objekty::Inventar inventar = Objekty::Inventar(3);

	Objekty::Nabytek::Skrin* skrin = new Objekty::Nabytek::Skrin("drevo", 120, 150, 120);
	Objekty::Nabytek::Skrin* skrin2 = new Objekty::Nabytek::Skrin("drevo", 120, 150, 120);
	Objekty::Nabytek::Stul* stul = new Objekty::Nabytek::Stul("dub", "modra");
	Objekty::Nabytek::Zidle* zidle = new Objekty::Nabytek::Zidle(true, true);

	inventar.Pridej(skrin);
	inventar.Pridej(stul);
	inventar.Pridej(zidle);

	inventar.VypisInventar();

	cout << "Nejvetsi inventarni cislo: " << inventar.NajdiLimit() << endl;


	//typeid(Objekty::Nabytek::Skrin).

#define out(x) cout << #x << " => " << x << endl;
#define outb(x) cout << #x << " => " << (x ? "y" : "n") << endl;
#define typed(x) out(typeid(x).name()); out(typeid(x).raw_name());

	typed(Objekty::Nabytek::Skrin);
	typed(int);
	typed(sizeof(char));

	cout << endl;

	typedef Objekty::Nabytek::Skrin Skrinka;
	typed(Skrinka);
	outb(typeid(Skrinka) == typeid(Objekty::Nabytek::Skrin));

	cout << endl;

	const Baze::Objekt* ukazatel = skrin;
	typed(ukazatel);
	outb(typeid(*ukazatel) == typeid(Objekty::Nabytek::Skrin));

	cout << endl;

	const Baze::Objekt& reference = *skrin;
	typed(reference);
	outb(typeid(reference) == typeid(Objekty::Nabytek::Skrin));

	cout << endl;

	outb(typeid(*skrin) == typeid(*skrin2));

	cout << endl;

	outb(typeid(Objekty::Nabytek::Skrin).before(typeid(Objekty::Nabytek::Stul)));

	system("pause");
}