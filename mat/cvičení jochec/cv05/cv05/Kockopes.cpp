#include <iostream>

using std::cout;
using std::endl;

class Baze
{
public:
	Baze() {}
	virtual ~Baze() {}

	virtual void vypis()
	{
		cout << "Baze" << endl;
	}
};

class Kralik : public Baze
{
public:
	Kralik() : Baze() {}
	~Kralik() {}

	void vypis() override
	{
		cout << "Kralik" << endl;
	}
};

class Kocka : virtual public Baze
{
public:
	Kocka() : Baze() {}
	~Kocka() {}

	void vypis() override
	{
		cout << "Kocka" << endl;
	}
};

class Pes : virtual public Baze
{
public:
	Pes() : Baze() {}
	~Pes() {}

	void vypis() override
	{
		cout << "Pes" << endl;
	}
};

class Kytka
{
public:
	Kytka() {}
	virtual ~Kytka() {}

	void vypis()
	{
		cout << "Kytka" << endl;
	}
};

class KockoPes : public Kocka, public Pes
{
public:

	KockoPes() : Kocka(), Pes() {}
	~KockoPes() {}

	void vypis() override
	{
		cout << "Kockopes" << endl;
	}
};

//===========================

int main(void)
{
	Baze b = Baze();
	Kralik k = Kralik();
	Kocka kocka = Kocka();
	Pes pes = Pes();
	KockoPes kp = KockoPes();

	cout << "Je kralik before baze?" << endl;
	if (typeid(k).before(typeid(b)))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je Kocka before Baze?" << endl;
	if (typeid(kocka).before(typeid(b)))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je Pes before Baze?" << endl;
	if (typeid(pes).before(typeid(b)))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je kockopes before Kocka?" << endl;
	if (typeid(kp).before(typeid(kocka)))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je kockopes before Pes?" << endl;
	if (typeid(kp).before(typeid(pes)))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je Kocka == Pes?" << endl;
	if (typeid(kocka) == typeid(pes))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "Je KockoPes == Pes?" << endl;
	if (typeid(kp) == typeid(pes))
		cout << "je" << endl;
	else cout << "neni" << endl;

	cout << "pretypovani" << endl;

	KockoPes kockoPes = KockoPes();


	return 0;
}