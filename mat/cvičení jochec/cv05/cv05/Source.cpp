#include <iostream>
#include <string>
#include <cstring>

#include <typeinfo>

using namespace std;

namespace Baze {
	class Objekt {
	public:
		Objekt(unsigned long invCislo) : invCislo(invCislo) { }

		unsigned long VratInventarniCislo() const { return invCislo; }
		virtual void VypisInformace() const = 0;
		friend bool operator<(const Objekt& oa, const Objekt& ob) {
			return oa.invCislo < ob.invCislo;
		}

	private:
		unsigned long invCislo;
	};
}

namespace Objekty {
	namespace Nabytek {

		class Skrin : public Baze::Objekt {
		public:
			Skrin(unsigned long invCislo, const string& material, const string& rozmery) : Objekt(invCislo), material(material), rozmery(rozmery) { }
			virtual void VypisInformace() const override {
				cout << "Skrin " << VratInventarniCislo() << " (" << material << ", " << rozmery << ")" << endl;
			}

		private:
			string material;
			string rozmery;
		};

		class Stul : public Baze::Objekt {
		public:
			Stul(unsigned long invCislo, const string& barva, const string& material) : Objekt(invCislo), barva(barva), material(material) { }
			virtual void VypisInformace() const override {
				cout << "Stul " << VratInventarniCislo() << " (" << barva << ", " << material << ")" << endl;
			}

		private:
			string barva;
			string material;
		};

		class Zidle : public Baze::Objekt {
		public:
			Zidle(unsigned long invCislo, bool otocna, bool ergonomicka) : Objekt(invCislo), otocna(otocna), ergonomicka(ergonomicka) { }
			virtual void VypisInformace() const override {
				cout << "Zidle " << VratInventarniCislo() << " (" << (otocna ? "otocna" : "staticka") << ", " << (ergonomicka ? "ergo" : "neergo") << ")" << endl;
			}

		private:
			bool otocna;
			bool ergonomicka;
		};

	}

	class Inventar {
	public:
		Inventar(size_t pocet) : kapacita(pocet), index(0), pole(new const Baze::Objekt*[pocet]) {
			//memset(pole, 0, sizeof(const Baze::Objekt*) * kapacita);
			for (size_t i = 0; i < kapacita; i++) {
				pole[i] = nullptr;
			}
		}

		~Inventar() {
			delete[] pole;
		}

		void Pridej(const Baze::Objekt* const objekt) {
			if (index >= kapacita) {
				throw exception("plne pole");
			}

			pole[index++] = objekt;
		}

		unsigned long NajdiLimit() const {
			if (index == 0) {
				throw exception("prazdne pole");
			}

			const Baze::Objekt* limitni = pole[0];
			for (size_t i = 1; i < index; i++) {
				if (*pole[i] < *limitni) {
					limitni = pole[i];
				}
			}

			return limitni->VratInventarniCislo();
		}

		void VypisInventar() const {
			for (size_t i = 0; i < index; i++){
				pole[i]->VypisInformace();
			}
		}

	private:
		const Baze::Objekt** const pole;
		size_t kapacita;
		size_t index;

	};

}


int main() {
	Objekty::Inventar inv(10);

	const Objekty::Nabytek::Skrin* const s1 = new Objekty::Nabytek::Skrin(123, "mat", "rozXrozXroz");
	const Objekty::Nabytek::Skrin* const s2 = new Objekty::Nabytek::Skrin(124, "mat", "rozXrozXroz");
	const Objekty::Nabytek::Zidle* const z1 = new Objekty::Nabytek::Zidle(10, true, false);
	const Objekty::Nabytek::Stul* const t1 = new Objekty::Nabytek::Stul(100, "bar", "mat");

	const Objekty::Nabytek::Skrin& sr = *s1;
	Objekty::Nabytek::Skrin& nsr = const_cast<Objekty::Nabytek::Skrin&>(sr);

	inv.Pridej(s1);
	inv.Pridej(z1);
	inv.Pridej(t1);

	inv.VypisInventar();
	cout << "Limit: " << inv.NajdiLimit() << endl;
	cout << endl << endl;


	////////////////////////////////////////////////////////////

	//typeid(Objekty::Nabytek::Skrin).

#define out(x) cout << #x << " =>" << x << endl;
#define outb(x) cout << #x << " =>" << (x ? "y" : "n") << endl;
#define typed(x) out(typeid(x).name()); out(typeid(x).raw_name());

	typed(Objekty::Nabytek::Skrin);
	typed(int);
	typed(sizeof(char));

	typedef Objekty::Nabytek::Skrin Skrinka;
	typed(Skrinka);
	outb(typeid(Skrinka) == typeid(Objekty::Nabytek::Skrin));

	const Baze::Objekt* ukazatel = s1;
	typed(*ukazatel);
	outb(typeid(*ukazatel) == typeid(Objekty::Nabytek::Skrin));

	const Baze::Objekt& reference  = *s1;

	typed(reference);
	outb(typeid(reference) == typeid(Objekty::Nabytek::Skrin));


	outb(typeid(*s1) == typeid(*s2));

	cout << endl;

	outb(typeid(Objekty::Nabytek::Skrin).before(typeid(Objekty::Nabytek::Skrin)));
	



	return 0;
}