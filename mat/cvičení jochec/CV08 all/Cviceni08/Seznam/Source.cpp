#include <iostream>
#include <list>
#include <algorithm>
#include <string>

using namespace std;

void printLists(const list<int> &l1, const list<int> &l2)
{
	cout << "list1: ";
	copy(l1.begin(), l1.end(), ostream_iterator<int>(cout, " "));
	cout << endl << "list2: ";
	copy(l2.begin(), l2.end(), ostream_iterator<int>(cout, " "));
	cout << endl << endl;
}


int main(int argc, char **argv)
{
	//vytvori dva prazdne seznamy
	list<int> list1, list2;

	//naplni oba seznamy prvky
	for (int i = 0; i < 6; i++)
	{
		list1.push_back(i);
		list2.push_back(i);
	}
	printLists(list1, list2);

	//vlozi vsechny prvky seznamu list1 pred prvni prvek seznamu list2 s hodnotou 3
	// - funkce find() vraci iterator prvniho prvku s hodnotou 3
	list2.splice(find(list2.begin(), list2.end(), //cilova pozice
		3), 
		list1); //zdrojovy seznam
	printLists(list1, list2);

	//presune prvni prvek na konec
	list2.splice(list2.end(), //cilova pozice
				list2, //zdrojovy seznam
				list2.begin()); //zdrojova pozice
	printLists(list1, list2);

	//seradi druhy seznam, priradi ho prvnimu a vyjme duplicity
	list2.sort();
	list1 = list2;
	list2.unique();
	printLists(list1, list2);
	
	//slouci oba serazene seznamy do prvniho
	list1.merge(list2);
	printLists(list1, list2);
}