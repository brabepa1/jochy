#include <iostream>
#include <set>
#include <functional>
#include <string>

template <class T>
class RuntimeCmp
{
public:
	enum cmp_mode {normal, reverse};
private: 
	cmp_mode mode;
public:
	//konstruktor pravidla razeni
	//- implicitni pravidlo pouziva hodnotu normal
	RuntimeCmp(cmp_mode m = normal) : mode(m) { }
	//porovnani prvku
	bool operator() (const T& t1, const T& t2) const
	{
		return mode == normal ? t1 < t2 : t2 < t1;
	}
	//porovnani pravidel razeni
	bool operator==(const RuntimeCmp& rc)
	{
		return mode == rc.mode;
	}
};