#include <iostream>
#include <set>
#include <functional>
#include <string>
#include "RuntimeCmp.h"

using namespace std;

void ukazka()
{
	set<int> c;

	c.insert(1);
	c.insert(2);
	c.insert(4);
	c.insert(5);
	c.insert(6);

	cout << "lower_bound(3): " << *c.lower_bound(3) << endl;
	cout << "upper_bound(3): " << *c.upper_bound(3) << endl;
	cout << "equal_range(3): " << *c.equal_range(3).first << " "
							   << *c.equal_range(3).second << endl;
	cout << endl;
	cout << "lower_bound(5): " << *c.lower_bound(5) << endl;
	cout << "upper_bound(5): " << *c.upper_bound(5) << endl;
	cout << "equal_range(5): " << *c.equal_range(5).first << " "
							   << *c.equal_range(5).second << endl;
	cout << endl;
}

void ukazka2()
{
	/* typ kolekce
		- bez duplicity
		- prvky maji celociselne hodnoty
		- sestupne razeni
	*/
	typedef set<int, greater<int> > IntSet;

	IntSet coll1; //prazdny kontejner

	//vlozi prvky v nahodnem poradi
	coll1.insert(4);
	coll1.insert(3);
	coll1.insert(5);
	coll1.insert(1);
	coll1.insert(6);
	coll1.insert(2);
	coll1.insert(5);

	//projde a vytiskne vsechny prvky
	IntSet::iterator pos;
	for (pos = coll1.begin(); pos != coll1.end(); ++pos)
	{
		cout << *pos << ' ';
	}
	cout << endl;

	//vlozi znovu hodnotu 4 a zpracuje navratovou hodnotu
	pair<IntSet::iterator, bool> status = coll1.insert(4);
	if(status.second)
	{
		cout << "hodnota 4 byla vlozena jako prvek "
			<< distance(coll1.begin(), status.first) + 1
			<< endl;
	}
	else
		cout << "hodnota 4 jiz existuje" << endl;
	
	//priradi prvky jine sade s vzestupnym razenim
	set<int> coll2(coll1.begin(), coll1.end());

	//vytiskne vsechny prvky kopie
	copy(coll2.begin(), coll2.end(),
		ostream_iterator<int>(cout, " "));
	cout << endl;

	//vyjme vsechny prvky po hodnotu 3
	coll2.erase(coll2.begin(), coll2.find(3));

	//vyjme vsechny prvky s hodnotou 5
	int num;
	num = coll2.erase(5);
	cout << num << " prvku vyjmuto" << endl;

	//vytiskne vsechny prvky
	copy(coll2.begin(), coll2.end(),
		ostream_iterator<int>(cout, " "));
	cout << endl;
}

//typ sady, ktery pouziva pravidlo razeni
typedef set<int, RuntimeCmp<int> > IntSet2;

//dopredna deklarace
void fill(IntSet2 &coll1);

void ukazka3()
{
	//vytvori, naplni a vytiskne sadu s normalnim razenim prvku
	// - pouzije implicitni pravidlo razeni
	IntSet2 coll1;
	fill(coll1);
	copy(coll1.begin(), coll1.end(),
		ostream_iterator<int>(cout, " "));
	cout << endl;

	//vytvori pravidlo razeni s opacnym pradim prvku
	RuntimeCmp<int> reverse_order(RuntimeCmp<int>::reverse);

	//vytvori, naplni a vytiskne sadu s opacnym razenim prvku
	IntSet2 coll2(reverse_order);
	fill(coll2);
	copy(coll2.begin(), coll2.end(),
		ostream_iterator<int>(cout, " "));
	cout << endl;

	//priradi prvky a pravidlo razeni
	coll1 = coll2;
	coll1.insert(3);
	copy(coll1.begin(), coll1.end(),
		ostream_iterator<int>(cout, " "));
	cout << endl;
	
	//jen pro jistotu
	if (coll1.value_comp() == coll2.value_comp())
	{
		cout << "kontejnery coll1 a coll2 maji stejnou podminku razeni"
			<< endl;
	}
	else
	{
		cout << "kontejnery coll1 a coll2 maji ruznou podminku razeni"
			<< endl;
	}
}

void fill(IntSet2 &coll1)
{
	//vlozi prvky v nahodnem poradi
	coll1.insert(4);
	coll1.insert(3);
	coll1.insert(5);
	coll1.insert(1);
	coll1.insert(6);
	coll1.insert(2);
	coll1.insert(5);
}

int main(int argc, char ** argv)
{
	ukazka();
	ukazka2();
	ukazka3();

}