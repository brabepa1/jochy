#include <iostream>

using namespace std;

class Pole{

public: //public aby to videlo v mainu Pole

	int* novePole; //tohle musi byt nahore, aby to videlo i potom

	Pole(size_t pocet){ // constructor
		
		novePole = new int[pocet];
		for (int i = 0; i < pocet; i++)
		{
			novePole[i] = 0;
		}
	
	}

	~Pole(){ //destructor
		delete[]novePole;
	}

	int& operator[](size_t index){ //to podtim je stejny, akorat const, takoze s tim polem nemuzu pracovat, treba k nemu neco pricist
	
		return novePole[index];
	
	}

	 const int& operator[](size_t index)const{

		return novePole[index];

	}
	

};

int main(){

	Pole p = Pole(5);

	cout<<	p[2] << endl;

	return 0;
}


