#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
#include <ctime> 
#include <cstdlib>
#include <functional>
#include <list>

using namespace std;

template <typename Function>
struct ProvedDetem : public unary_function < Rodina, void > {

	ProvedDetem() { }
	ProvedDetem(Function function) : _function(function) { }

	void operator()(Rodina& rodina) {
		for (list<Osoba>::iterator it = rodina.deti.begin(); it != rodina.deti.end(); it++) {
			_function(*it);
		}
	}

private:
	Function _function;
};

struct Osoba{

	string _jmeno;
	string _prijmeni;
	char _pohlavi;

	Osoba(){};

	Osoba(string jmeno, string prijmeni, char pohlavi){

		_jmeno = jmeno;
		_prijmeni = prijmeni;
		_pohlavi = pohlavi;
		
	};

	void vypis(){

		cout << "Jmeno: " << _jmeno << "Prijmeni: " << _prijmeni << "Pohlavi: " << _pohlavi << endl;
	}

	char getPohlavi(){
		return _pohlavi;
	}

};

struct Rodina{

	pair <Osoba, Osoba> _rodice;
	list <Osoba> _deti;

	Rodina(){};
	
	Rodina(pair <Osoba, Osoba> rodice, list <Osoba> deti){

		_rodice = rodice;
		_deti = deti;		

	};


};


bool jeMuz(Osoba o){

	return o.getPohlavi == 'm';
	
}

bool jeZena(Osoba o){

	return o.getPohlavi == 'z';
}


Osoba generujMuze(){

	string jmeno;
	string prijmeni;	
	char pohlavi = 'm';

	jmeno = rand() % 10000 + 1;
	prijmeni = rand() % 100000 + 1;

	return Osoba(jmeno,prijmeni,pohlavi);

	
}

Osoba generujZenu(){

	string jmeno;
	string prijmeni;
	char pohlavi = 'z';	

	jmeno = rand() % 10000 + 1;
	prijmeni = rand() % 100000 + 1;

	return Osoba(jmeno, prijmeni, pohlavi);
	
}

Rodina generujRodinu(Osoba muz, Osoba zena){

	pair <Osoba, Osoba> rodina;
	list <Osoba> deti;

	rodina.first = muz;
	rodina.second = zena;


	int pocetDeti = 0;
	int dite = rand() %100;
	
	if (dite > 40  && dite <=80)
	{
		pocetDeti = 1;
	}
		else
	{
		pocetDeti = 2;
	}

}


int main(){

	srand(time(NULL));

	vector<Osoba> osoby(10);

	generate(osoby.begin(), osoby.end() -5, generujMuze);
	generate(osoby.begin() + 5, osoby.end(), generujZenu);


	for_each(osoby.begin(), osoby.end(), mem_fun_ref(&Osoba::vypis));

	vector<Osoba> muzi(5);

	remove_copy_if(osoby.begin(), osoby.end(), muzi.begin(), jeMuz);

	for_each(muzi.begin(), muzi.end(), mem_fun_ref(&Osoba::vypis));


	vector<Osoba> zeny(5);

	remove_copy_if(osoby.begin(), osoby.end(), zeny.begin(), jeZena);

	for_each(zeny.begin(), zeny.end(), mem_fun_ref(&Osoba::vypis));



	system("pause");
	return 0;
}