/*
1. Vytvo�te strukturu Osoba(string jmeno, string prijmeni, char pohlavi(�m� / �z�)).M�jte
vector Osob a vytvo�te v n�m 5 mu�� a 5 �en.Osoby vygenerujte pomoc� funk�n�ho
objektu a algoritmu generate / generate_n.
2. Pomoc� vhodn�ho algoritmu separujte mu�e a �eny do dvou samostatn�ch vektor�.
3. Pomoc� algoritmu transform zpracujte v�dy p�r m��e a �eny a vytvo�te strukturu
Rodina(s atributy pair<Osoba, Osoba> rodice, list<Osoba> deti).S
pravd�podobnost� 40 % p�r nebude m�t ��dn� d�t�, s p. 40 % bude m�t jedno d�t�, s
p 20 % budou m�t dv� d�ti.Jm�na, p��jmen� a pohlav� d�t� nagenerujte, p��jmen�
p�evezm�te od otce.
4. Vypi�te rodiny a jejich d�ti na obrazovku.
5. Odstra�te z vektoru v�echny bezd�tn� p�ry.
6. Vypi�te rodiny a jejich d�ti na obrazovku.
7. Vytvo�te funk�n� objekt, kter� p�ed�te funk�n�mu objektu ProvedDetem(a ten
algoritmu for_each).Tento funk�n� objekt ulo�� v�echny d�ti do nov�ho vektoru.
8. Vypi�te nov� vektor d�t�.
Zajist�te, aby p�i ka�d�m spu�t�n� programu se vygenerovaly jin� osoby.
*/
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>

using namespace std;

struct Osoba {
	string jmeno;
	string prijmeni;
	char pohlavi;
	Osoba(string j, string p, char po) : jmeno(j), prijmeni(p), pohlavi(po) {}
};

ostream& operator<<(ostream& os, const Osoba& osoba) {
	os << osoba.jmeno << " " << osoba.prijmeni << " (" << osoba.pohlavi << ")";
	return os;
}

struct GeneratorOsob {
	GeneratorOsob() : i(0) {}
	Osoba operator()() {
		char pohlavi = i++ % 2 ? 'm' : 'z';
		return Osoba(nahodny(), nahodny(), pohlavi);
	}
	static string nahodny() {
		string s;
		for (int i = 0, c = rand() % 10 + 3; i < c; i++) {
			s.push_back(rand() % ('z' - 'a') + 'a');
		}
		s[0] = toupper(s[0]);
		return s;
	}
private:
	int i;
};

struct jeMuz : public unary_function<Osoba, bool> {
	bool operator()(const Osoba& os) const {
		return os.pohlavi == 'm';
	}
};

/*bool jeZena(const Osoba& os) {
return !jeMuz(os);
}*/

struct Rodina {
	pair<Osoba, Osoba> rodice;
	list<Osoba> deti;
	Rodina(pair<Osoba, Osoba> p) : rodice(p) {}
};

ostream& operator<<(ostream& os, const Rodina& r) {
	os << r.rodice.first << " + " << r.rodice.second << ": ";
	copy(r.deti.begin(), r.deti.end(), ostream_iterator<Osoba>(os, ", "));
	return os;
}

struct GeneratorRodin {
	Rodina operator()(const Osoba& os1, const Osoba& os2) {
		//Rodina r(make_pair(os1, os2));
		Rodina r(pair<Osoba, Osoba>(os1, os2));
		int random = rand() % 10;
		if (random < 4) {}
		else if (random < 8) {
			pridejDite(r, os1);
		}
		else {
			pridejDite(r, os1);
			pridejDite(r, os1);
		}
		return r;
	}

	void pridejDite(Rodina& r, const Osoba& os) {
		r.deti.push_back(Osoba(GeneratorOsob::nahodny(), os.prijmeni, rand() % 2 ? 'm' : 'z'));
	}
};

template <typename Function>
struct ProvedDetem : public unary_function < Rodina, void > {
	ProvedDetem() { }
	ProvedDetem(Function function) : _function(function) { }
	void operator()(Rodina& rodina) {
		for (list<Osoba>::iterator it = rodina.deti.begin(); it != rodina.deti.end(); it++) {
			_function(*it);
		}
	}
private:
	Function _function;
};

struct Separator {
	Separator(vector<Osoba>& m, vector<Osoba>& z) : muzi(m), zeny(z) {}
	void operator()(const Osoba& os) {
		if (os.pohlavi == 'm') {
			muzi.push_back(os);
		}
		else {
			zeny.push_back(os);
		}
	}
private:
	vector<Osoba>& muzi;
	vector<Osoba>& zeny;
};

int main() {
	srand(time(nullptr));
	
	vector<Osoba> vect;
	
	generate_n(back_inserter(vect), 10, GeneratorOsob());
	copy(vect.begin(), vect.end(), ostream_iterator<Osoba>(cout, "\n"));
	cout << endl;

	vector<Osoba> muzi, zeny;

	//remove_copy_if(vect.begin(), vect.end(), back_inserter(muzi), jeZena);

	//remove_copy_if(vect.begin(), vect.end(), back_inserter(muzi), not1(jeMuz()));
	//remove_copy_if(vect.begin(), vect.end(), back_inserter(zeny), jeMuz());

	//copy_if(vect.begin(), vect.end(), back_inserter(muzi), jeMuz());
	//copy_if(vect.begin(), vect.end(), back_inserter(zeny), not1(jeMuz()));

	for_each(vect.begin(), vect.end(), Separator(muzi, zeny));

	copy(muzi.begin(), muzi.end(), ostream_iterator<Osoba>(cout, "\n"));
	copy(zeny.begin(), zeny.end(), ostream_iterator<Osoba>(cout, "\n"));
	cout << endl;
	
	vector<Rodina> rodiny;
	
	transform(muzi.begin(), muzi.end(), zeny.begin(), back_inserter(rodiny), GeneratorRodin());
	
	copy(rodiny.begin(), rodiny.end(), ostream_iterator<Rodina>(cout, "\n"));
	cout << endl;
	
	/*vector<Rodina>::iterator*/auto it = remove_if(rodiny.begin(), rodiny.end(), [](const Rodina& r) { return r.deti.empty(); });
	rodiny.erase(it, rodiny.end());
	
	copy(rodiny.begin(), rodiny.end(), ostream_iterator<Rodina>(cout, "\n"));
	cout << endl;
	
	vector<Osoba> deti;
	
	for_each(rodiny.begin(), rodiny.end(), ProvedDetem<Separator>(Separator(deti, deti)));
	
	copy(deti.begin(), deti.end(), ostream_iterator<Osoba>(cout, "\n"));
	cout << endl;
	return 0;
}