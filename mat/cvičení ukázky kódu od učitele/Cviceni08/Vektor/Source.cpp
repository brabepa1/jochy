#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void Ukol()
{
	/*
	Vytvo�te strukturu Osoba obsahuj�c� jm�no (std::string) a p��jmen� (std::string). 
	Vytvo�te vektor Osob a napl�te jej n�kolika r�zn�mi prvky. Prvky vypi�te na obrazovku.

	Vytvo�te mapu, kde kl��em je Osoba a hodnotou je cel� ��slo (rodn� ��slo). Vlo�te do mapy n�kolik
	polo�ek. Vyhledejte rodn� ��slo podle konkr�tn� osoby (implementujte operator< a operator== do struktury Osoba).
	*/
}

int main(int argc, char ** argv)
{
	Ukol();
	//vytvori prazdny vektor pro retezce
	vector<string> sentence;

	//vyhradi pamet pro pet prvku, aby nedoslo k nove alokaci
	sentence.reserve(5);

	//pripoji prvky
	sentence.push_back("Ahoj,");
	sentence.push_back("jak");
	sentence.push_back("se");
	sentence.push_back("mas");
	sentence.push_back("?");

	//vytiskne prvky oddelene mezerami
	copy(sentence.begin(), sentence.end(),
		ostream_iterator<string>(cout, " "));
	cout << endl;

	//vytiskne "technicke udaje"
	cout << "  max_size(): " << sentence.max_size() << endl;
	cout << "  size():     " << sentence.size()		<< endl;
	cout << "  capacity:   " << sentence.capacity() << endl;

	//zameni druhy a ctvrty prvek
	swap(sentence[1], sentence[3]);

	//vlozi prvek "vzdy" pred prvek "?"
	sentence.insert(find(sentence.begin(), sentence.end(), "?"), "vzdy");

	//priradi "!" poslednimu prvku
	sentence.back() = "!";

	//vytiskne prvky oddelene mezerami
	copy(sentence.begin(), sentence.end(),
		ostream_iterator<string>(cout, " "));
	cout << endl;

	//vytiskne "technicke udaje"
	cout << "  max_size(): " << sentence.max_size() << endl;
	cout << "  size():     " << sentence.size()		<< endl;
	cout << "  capacity:   " << sentence.capacity() << endl;

}