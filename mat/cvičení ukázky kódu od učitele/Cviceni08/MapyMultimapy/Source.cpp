#include <iostream>
#include <map>
#include <string>
#include <iomanip>
#include <algorithm>

using namespace std;

void MapaJakoAsociativniPole()
{
	/* vytvori mapu/asociativni pole
		- klice maji typ string
		- hodnoty maji typ float
	*/

	typedef map<string, float> StringFloatMap;

	StringFloatMap stocks; //vytvori prazdny kontejner

	//vlozi prvky
	stocks["BASF"] = 369.50f;
	stocks["VW"] = 413.50f;
	stocks["Daimler"] = 819.00f;
	stocks["BMW"] = 834.00f;
	stocks["Siemens"] = 842.20f;

	//vytiskne vsechny prvky
	StringFloatMap::iterator pos;
	for (pos = stocks.begin(); pos != stocks.end(); ++pos)
	{
		cout << "akcie: " << pos->first << "\t"
			<< "cena: " << pos->second << endl;
	}
	cout << endl;

	//prudky vzestup (zdvojnasobi vsechny ceny)
	for (pos = stocks.begin(); pos != stocks.end(); ++pos)
	{
		pos->second *= 2;
	}

	//vytiskne vsechny prvky
	for (pos = stocks.begin(); pos != stocks.end(); ++pos)
	{
		cout << "akcie: " << pos->first << "\t"
			<< "cena: " << pos->second << endl;
	}
	cout << endl;

	/* prejmenuje klic z "VW" na "Volkswagen"
		- je to  mozne pouze vymenou prvku
	*/
	stocks["Volkswagen"] = stocks["VW"];
	stocks.erase("VW");

	//vytiskne vsechny prvky
	for (pos = stocks.begin(); pos != stocks.end(); ++pos)
	{
		cout << "akcie: " << pos->first << "\t"
			<< "cena: " << pos->second << endl;
	}
	cout << endl;
}
void MultimapaJakoSlovnik()
{
	
	//definuje multimapu jako slovnik typu string/string
	typedef multimap<string, string> StrStrMMap;

	//vytvori prazdny slovnik
	StrStrMMap dict;

	//vlozi prvky v nahodnem poradi
	dict.insert(make_pair("day", "den"));
	dict.insert(make_pair("strange", "cizi"));
	dict.insert(make_pair("car", "auto"));
	dict.insert(make_pair("smart", "elegantni"));
	dict.insert(make_pair("trait", "vlastnost"));
	dict.insert(make_pair("strange", "divny"));
	dict.insert(make_pair("smart", "chytry"));
	dict.insert(make_pair("smart", "bystry"));
	dict.insert(make_pair("clever", "chytry"));
	
	//vytiskne vsechny prvky
	StrStrMMap::iterator pos;
	cout.setf(ios::left, ios::adjustfield);
	cout << ' ' << setw(10) << "anglicky "
		<< "cesky " << endl;
	cout << setfill('-') << setw(20) << " "
		<< setfill('-') << endl;
	for (pos = dict.begin(); pos != dict.end(); ++pos)
	{
		cout << ' ' << setw(10) << pos->first.c_str() << pos->second << endl;
	}
	cout << endl;

	//vytiskne vsechny hodnoty s klicem "smart"
	string word("smart");
	cout << word << ": " << endl;
	for (pos = dict.lower_bound(word); 
		pos != dict.upper_bound(word); ++pos)
	{
		cout << "     " << pos->second << endl;
	}

	//vytiskne vsechny hodnoty s klicem "smart"
	word = ("chytry");
	cout << word << ": " << endl;
	for (pos = dict.lower_bound(word); 
		pos != dict.upper_bound(word); ++pos)
	{
		cout << "     " << pos->second << endl;
	}
}
template <class K, class V>
class value_equals
{
private:
	V value;
public:
	//konstruktor (inicializuje hodnotu pro porovnani)
	value_equals(const V& v) : value(v)
	{
	}

	//porovnavani
	bool operator() (pair<const K, V> elem)
	{
		return elem.second == value;
	}
};

void NalezeniPrkvuSUrcitouHodnotou()
{
	typedef map<float, float> FloatFloatMap;
	FloatFloatMap coll;
	FloatFloatMap::iterator pos;

	//naplni kontejner
	coll[1] = 7;
	coll[2] = 4;
	coll[3] = 2;
	coll[4] = 3;
	coll[5] = 6;
	coll[6] = 1;
	coll[7] = 3;

	//hleda prvek s klicem 3.0
	pos = coll.find(3.0); //logaritmicka slozitost
	if(pos != coll.end())
	{
		cout << pos->first << ": "
			<< pos->second << endl;
	}
	
	//hleda prvek s klicem 3.0
	pos = find_if(coll.begin(), coll.end(),
		value_equals<float, float>(3.0));
	if(pos != coll.end())
	{
		cout << pos->first << ": "
			<< pos->second << endl;
	}
}
int main(int argc, char ** argv)
{
	MapaJakoAsociativniPole();
	MultimapaJakoSlovnik();
	NalezeniPrkvuSUrcitouHodnotou();
}