#include <iostream>
#include <deque>
#include <string>
#include <algorithm>

using namespace std;

int main(int argc, char ** argv)
{
	//vytvori prazdnou obousmernou frontu pro retezce
	deque<string> coll;

	//ulozi nekolik prvku
	coll.assign(3, string("retezec"));
	coll.push_back("posledni retezec");
	coll.push_front("prvni retezec");

	//vytiskne prvky oddelene novymi radky
	copy(coll.begin(), coll.end(),
		ostream_iterator<string>(cout, "\n"));
	cout << endl;

	//vyjme prvni a posledni prvek 
	coll.pop_front();
	coll.pop_back();

	//vlozi retezec "jiny"do vsech prvku krome prvniho
	for (unsigned i = 1; i < coll.size(); ++i)
	{
		coll[i] = "jiny " + coll[i];
	}

	//zmeni velikost na ctyri prvky
	coll.resize(4, "retezec zmeny velikosti");

	//vytiskne prvky oddelene novymi radky
	copy(coll.begin(), coll.end(),
		ostream_iterator<string>(cout, "\n"));
	cout << endl;
}