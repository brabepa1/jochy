#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>

using namespace std;

// funkce pro vypis prvku na obrazovku
void funkce(int i) {
	cout << i << endl;
}

// funkce vracejici zapornou hodnotu prvku
int funkce2(int i) {
	return -i;
}

// jednoduchy funkcni objekt
struct funkcniObjekt {
	funkcniObjekt(int parametr) : parametr(parametr) { }

	void operator()(int i) {
		cout << "funkcniObjekt " << (a + i) << endl;
	}

private:
	int parametr;
};

int main() {
	vector<int> vect = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	// vypis vsech prvku na obrazovku pomoci ukazatele na funkci
	for_each(vect.begin(), vect.end(), funkce);
	// vypis vsech prvku na obrazovku pomoci funkcniho objektu;
	for_each(vect.begin(), vect.end(), funkcniObjekt(100));

	// transformace prvku pomoci ukazatele na funkci (funkce2) a vypis vysledku na obrazovku
	transform(vect.begin(), vect.end(), ostream_iterator<int>(cout, " "), funkce2);

	vector<int> v2;
	
	// vkladani prvku do noveho kontejneru
	//v2.resize(vect.size());
	//transform(vect.begin(), vect.end(), v2.begin(), funkce2);

	// vkladani prvku pomoci vkladaciho iteratoru
	// ukazka lambda funkce (anonymni funkce) 
	transform(vect.begin(), vect.end(), back_inserter(v2), [](int i) {return -i; });

	// vycisteni vektoru
	vect.clear();

	// generovani 10 prvku s hodnotami 1000, 1001, 1002, ...
	int i = 1000;
	generate_n(back_inserter(vect), 10, [&i]() { return i++; });

	// rotace - 4. prvek se stane novym prvnim prvkem
	rotate(vect.begin(), vect.begin() + 3, vect.end());

	vector<int> v3 = { 0, 1, 0, 2, 0, 0, 3, 4, 5, 0, 6, 0, 7, 8, 9, 10 };

	// ukazka mazani, algoritmus pouze presouva prvky a vraci novy konec rozsahu
	auto it = remove(v3.begin(), v3.end(), 0);
	v3.erase(it, v3.end());

	return 0;
}