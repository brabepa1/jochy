#include <cstring>
#include <string>

template<typename Klic, typename Hodnota>
class Par {
public:
	Par(Klic klic, Hodnota hodnota);
	~Par();

	Klic& vratKlic();
	Hodnota& vratHodnota();

	bool operator==(const Par<Klic, Hodnota> druhy);
	bool operator!=(const Par<Klic, Hodnota> druhy);
private:
	Klic klic;
	Hodnota hodnota;
};

template<typename Klic, typename Hodnota>
Par<Klic, Hodnota>::Par(Klic klic, Hodnota hodnota) {
	this->hodnota = hodnota;
	this->klic = klic;
}

template<typename Klic, typename Hodnota>
Par<Klic, Hodnota>::~Par() {
}

template<typename Klic, typename Hodnota>
Klic& Par<Klic, Hodnota>::vratKlic() {
	return klic;
}

template<typename Klic, typename Hodnota>
Hodnota& Par<Klic, Hodnota>::vratHodnota() {
	return hodnota;
}

template<typename Klic, typename Hodnota>
bool Par<Klic, Hodnota>::operator==(const Par<Klic, Hodnota> druhy) {
	return klic == druhy.klic && hodnota == druhy.hodnota;
}

template<typename Klic, typename Hodnota>
bool Par<Klic, Hodnota>::operator!=(const Par<Klic, Hodnota> druhy) {
	//return !(*this == druhy);
	//return !operator==(druhy);
	return klic != druhy.klic || hodnota != druhy.hodnota;
}

//////////////////////////////
/// parc. specializace Klic*, Hodnota*


template<typename Klic, typename Hodnota>
class Par<Klic*,Hodnota*> {
public:
	Par(Klic* klic, Hodnota* hodnota);
	~Par();

	Klic& vratKlic();
	Hodnota& vratHodnota();

	bool operator==(const Par<Klic*, Hodnota*> druhy);
	bool operator!=(const Par<Klic*, Hodnota*> druhy);
private:
	Klic* klic;
	Hodnota* hodnota;
};

template<typename Klic, typename Hodnota>
Par<Klic*, Hodnota*>::Par(Klic* klic, Hodnota* hodnota) {
	this->hodnota = hodnota;
	this->klic = klic;
}

template<typename Klic, typename Hodnota>
Par<Klic*, Hodnota*>::~Par() {
}

template<typename Klic, typename Hodnota>
Klic& Par<Klic*, Hodnota*>::vratKlic() {
	return *klic;
}

template<typename Klic, typename Hodnota>
Hodnota& Par<Klic*, Hodnota*>::vratHodnota() {
	return *hodnota;
}

template<typename Klic, typename Hodnota>
bool Par<Klic*, Hodnota*>::operator==(const Par<Klic*, Hodnota*> druhy) {
	return *klic == *druhy.klic && *hodnota == *druhy.hodnota;
}

template<typename Klic, typename Hodnota>
bool Par<Klic*, Hodnota*>::operator!=(const Par<Klic*, Hodnota*> druhy) {
	//return !(*this == druhy);
	//return !operator==(druhy);
	return *klic != *druhy.klic || *hodnota != *druhy.hodnota;
}

/////////////////////////////
// Explicitni spec. char*,char*

template<>
class Par<char*,char*> {
public:
	Par(char* klic, char* hodnota);
	~Par();

	char*& vratKlic();
	char*& vratHodnota();

	bool operator==(const Par<char*, char*> druhy);
	bool operator!=(const Par<char*, char*> druhy);
private:
	char* klic;
	char* hodnota;
};

Par<char*, char*>::Par(char* klic, char* hodnota) {
	this->hodnota = hodnota;
	this->klic = klic;
}

Par<char*, char*>::~Par() {
}

char*& Par<char*, char*>::vratKlic() {
	return klic;
}

char*& Par<char*, char*>::vratHodnota() {
	return hodnota;
}

bool Par<char*, char*>::operator==(const Par<char*, char*> druhy) {
	//return std::strcmp(klic, druhy.klic) == 0 && std::strcmp(hodnota, druhy.hodnota) == 0;
	//return !std::strcmp(klic, druhy.klic) && !std::strcmp(hodnota, druhy.hodnota);
	return std::string(klic) == druhy.klic && std::string(hodnota) == druhy.hodnota;
}

bool Par<char*, char*>::operator!=(const Par<char*, char*> druhy) {
	//return !(*this == druhy);
	return !operator==(druhy);
}