#include <iostream>

typedef unsigned int rozmer_t;

class Tabulka {

public:
	Tabulka(rozmer_t r = 3, rozmer_t s = 3) : radku(r), sloupcu(s)  {
		pole = new int*[radku];
		for (rozmer_t i = 0; i < radku; i++) {
			pole[i] = new int[sloupcu];
		}

		int k = 0;
		for (rozmer_t i = 0; i < radku; i++) {
			for (rozmer_t j = 0; j < sloupcu; j++) {
				pole[i][j] = k++;
			}
		}
	}

	~Tabulka() {
		for (rozmer_t i = 0; i < radku; i++) {
			delete[] pole[i];
		}
		delete[] pole;
	}

	Tabulka(const Tabulka& ref) {
		radku = ref.radku;
		sloupcu = ref.sloupcu;

		pole = new int*[radku];
		for (rozmer_t i = 0; i < radku; i++) {
			pole[i] = new int[sloupcu];
		}

		for (rozmer_t i = 0; i < radku; i++) {
			for (rozmer_t j = 0; j < sloupcu; j++) {
				pole[i][j] = ref.pole[i][j];
			}
		}
	}

	Tabulka& operator=(const Tabulka& ref) {
		if (this == &ref) {
			return *this;
		}
		
		Tabulka::~Tabulka();

		radku = ref.radku;
		sloupcu = ref.sloupcu;

		pole = new int*[radku];
		for (rozmer_t i = 0; i < radku; i++) {
			pole[i] = new int[sloupcu];
		}

		for (rozmer_t i = 0; i < radku; i++) {
			for (rozmer_t j = 0; j < sloupcu; j++) {
				pole[i][j] = ref.pole[i][j];
			}
		}

		return *this;
	}

	Tabulka operator+(const Tabulka& ref) const {
		Tabulka n(radku, sloupcu);

		for (rozmer_t i = 0; i < radku; i++) {
			for (rozmer_t j = 0; j < sloupcu; j++) {
				n.pole[i][j] = pole[i][j] + ref.pole[i][j];
			}
		}

		return n;
	}

private:
	rozmer_t radku;
	rozmer_t sloupcu;
	int** pole;
};

int main() {
	Tabulka a, b, c;

	a = b = c;
	a = b + c;

	return 0;
}