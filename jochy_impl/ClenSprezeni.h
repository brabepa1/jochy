/* 
 * File:   ClenZprezeni.h
 * Author: pavel
 *
 * Created on 20. prosinec 2015, 16:11
 */

#ifndef CLENZPREZENI_H
#define	CLENZPREZENI_H

class ClenSprezeni {
public:
    virtual void Vypis() = 0;

    friend bool operator<(const ClenSprezeni& a, const ClenSprezeni& b) {
        return a.rank < b.rank;
    }

    double getRank()const {
        return rank;
    }

protected:
    double rank;
};

#endif	/* CLENZPREZENI_H */

