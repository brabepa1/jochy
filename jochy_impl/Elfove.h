#ifndef ELFOVE_H
#define	ELFOVE_H

class Elfove {
private:
    static std::vector<std::string> split(std::string s);
    static int stringToInt(std::string s);
public:
    static Sprezeni sestav(std::string zadani);
};

#endif

