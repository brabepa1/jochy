#include <iostream>
#include "TaznaSila.h"

using namespace std;

Sob::Sob() {
    rank = 0;
    taznyVykon = 500.0;
}

void Sob::Vypis() {
    cout << "Sob: \t\t\t" << taznyVykon << "\t" << rank << endl;
}

Los::Los() {
    rank = 1;
    taznyVykon = 300.0;
}

void Los::Vypis() {
    cout << "Los: \t\t\t" << taznyVykon << "\t" << rank << endl;
}

StribrnyKralicek::StribrnyKralicek() {
    rank = 2;
    taznyVykon = 10.0;
}

void StribrnyKralicek::Vypis() {
    cout << "StribrnyKralicek: \t" << taznyVykon << "\t" <<  rank << endl;
}

double TaznaSila::getTaznyVykon() const {
    return taznyVykon;
}
