/* 
 * File:   Sprezeni.h
 * Author: pavel
 *
 * Created on 21. prosinec 2015, 14:07
 */

#ifndef SPREZENI_H
#define	SPREZENI_H

template<typename T>
struct DereferenceLess : public std::binary_function < T, T, bool > {

    bool operator()(T p1, T p2) {
        return *p1 < *p2;
    }
};

class Sprezeni{
 friend class Elfove;
private:
    UsporadanyKontejner<ClenSprezeni* ,DereferenceLess<ClenSprezeni*> > clenoveSprezeni;
    Sprezeni(){};
public:
    double ZjistiVykon() const;
    double ZjistiVahu() const;
    void Vypis() const;
    void OdstranNadvahu();
    bool JeLetuSchopny() const;
};

#endif	/* SPREZENI_H */

