#include <stdio.h>
#include <stdlib.h>

#include "TaznaSila.cpp"
#include "Vuz.cpp"
#include "UsporadanyKontejner.cpp"
#include "Sprezeni.cpp"
#include "Elfove.cpp"

using namespace std;

void printMenu() {
    cout << endl << "----------------------------------------------" << endl;
    cout << "Santovo menu:" << endl;
    cout << "\t" << "1) zjisti vykon" << endl;
    cout << "\t" << "2) zjisti vahu" << endl;
    cout << "\t" << "3) vypis" << endl;
    cout << "\t" << "4) odstran nadvahu" << endl;
    cout << "\t" << "5) je letu schopny" << endl;
    cout << "\t" << "6) seru na to jdu se vozrat" << endl;
    cout << "----------------------------------------------" << endl;

}

int main(int argc, char** argv) {
    try {
        cout << "Zadej sprezeni: " << endl;
        string strSprezeni;
        cin >> strSprezeni;
        Sprezeni sprezeni = Elfove::sestav(strSprezeni);

        while (true) {
            printMenu();
            cout << "Tvoje volba: ";
            int option;
            while (getchar() != '\n');
            if (scanf("%d", &option) != 1) {
                cout << "Neplatna volba! Zkus to znovu" << endl;
                continue;
            }
            cout << endl;
            switch (option) {
                case 1:
                    cout << "vykon: " << sprezeni.ZjistiVykon() << endl;
                    break;
                case 2:
                    cout << "vaha: " << sprezeni.ZjistiVahu() << endl;
                    break;
                case 3:
                    sprezeni.Vypis();
                    break;
                case 4:
                    sprezeni.OdstranNadvahu();
                    break;
                case 5:
                    cout << "letu schopne: " << (sprezeni.JeLetuSchopny() == 0 ? "NE" : "ANO") << endl;
                    break;
                case 6:
                    return (EXIT_SUCCESS);
                default:
                    cout << "Neplatna volba! Zkus to znovu" << endl;
            }
        }
    } catch (string s) {
        cout << s << endl;
        return EXIT_FAILURE;
    }
        
    return (EXIT_SUCCESS);
}

