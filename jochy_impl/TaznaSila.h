/* 
 * File:   TaznaSila.h
 * Author: pavel
 *
 * Created on 20. prosinec 2015, 16:04
 */

#ifndef TAZNASILA_H
#define	TAZNASILA_H

#include "ClenSprezeni.h"

class TaznaSila : public ClenSprezeni{
public:
    double taznyVykon;
    double getTaznyVykon() const;
    virtual void Vypis() = 0;
};

class Sob : public TaznaSila{
public:
    Sob();
    virtual void Vypis();
};

class Los : public TaznaSila{
public:
    Los();
    virtual void Vypis();
};

class StribrnyKralicek : public TaznaSila{
public:
    StribrnyKralicek();
    virtual void Vypis();
};

#endif	/* TAZNASILA_H */

