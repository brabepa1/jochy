/* 
 * File:   Vuz.h
 * Author: pavel
 *
 * Created on 20. prosinec 2015, 16:14
 */

#ifndef VUZ_H
#define	VUZ_H

#include "ClenSprezeni.h"

class Vuz : public ClenSprezeni {
protected:
    double vahaVuz;
public:
    Vuz();
    virtual void Vypis() = 0;
    virtual double getVaha() = 0;
};

class Santa : public Vuz {
    double vahaSanta;
public:
    Santa();
    virtual void Vypis();
    virtual double getVaha();
};

class Naklad : public Vuz {
    double vahaDarku;
public:
    Naklad(double vahaDarku);
    virtual void Vypis();
    virtual double getVaha();
};

class Technicky : public Vuz {
    double vahaTechnicky;
public:
    Technicky();
    virtual void Vypis();
    virtual double getVaha();
};

#endif	/* VUZ_H */

