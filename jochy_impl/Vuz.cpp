#include <cstdlib>
#include <iostream>

#include "Vuz.h"

using namespace std;

Vuz::Vuz() {
    vahaVuz = 50;
}

Santa::Santa() {
    rank = 3;
    vahaSanta = 150;
}

void Santa::Vypis() {
    cout << "Santa: \t\t\t" << getVaha() << "\t " << rank << endl;
}

double Santa::getVaha() {
    return vahaSanta + vahaVuz;
}

Naklad::Naklad(double vahaDarku) {
    rank = 5 + 1 / vahaDarku;
    this->vahaDarku = vahaDarku;
}

void Naklad::Vypis() {
    cout << "Naklad(" << vahaDarku << "): \t\t" << getVaha() << "\t " << rank << endl;
}

double Naklad::getVaha() {
    return vahaDarku + vahaVuz;
}

Technicky::Technicky() {
    rank = 4;
    vahaTechnicky = 100;
}

void Technicky::Vypis() {
    cout << "Technicky: \t" << getVaha() << "\t " << rank << endl;
}

double Technicky::getVaha() {
    return vahaTechnicky;
}



