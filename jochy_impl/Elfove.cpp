/* 
 * File:   Elfove.cpp
 * Author: pavel
 * 
 * Created on 21. prosinec 2015, 14:57
 */

#include "Elfove.h"
#include "Sprezeni.h"
#include "UsporadanyKontejner.h"
#include "TaznaSila.h"
#include "Vuz.h"

#include <vector>
#include <string>
#include<string.h>
#include <sstream> 

using namespace std;

Sprezeni Elfove::sestav(std::string zadani) {
    Sprezeni sprezeni;
    vector<string> tokens = split(zadani);
    for (unsigned int i = 0; i < tokens.size(); i++) {
        string token = tokens[i];
        if (token.compare("S")== 0) {
            sprezeni.clenoveSprezeni.Vloz(new Sob());
            continue;
        }
        if (token.compare("L")== 0) {
            sprezeni.clenoveSprezeni.Vloz(new Los());
            continue;
        }
        if (token.compare("K")== 0) {
            sprezeni.clenoveSprezeni.Vloz(new StribrnyKralicek());
            continue;
        }
        if (token.compare("X")== 0) {
            sprezeni.clenoveSprezeni.Vloz(new Santa());
            continue;
        }
        if (token.compare("T") == 0) {
            sprezeni.clenoveSprezeni.Vloz(new Technicky());
            continue;
        }
        //stringToIn vyhodi vyjimku v pripade vsech neplatnych tokenu
        int tmp =stringToInt(token);
        sprezeni.clenoveSprezeni.Vloz(new Naklad((double)tmp));
    }
    return sprezeni;
}

vector<string> Elfove::split(std::string s) {
    char * pch;
    char * str = new char[s.size()];
    strcpy(str, s.c_str());
    pch = strtok(str, ",");
    vector<string> tokens;
    while (pch != NULL) {
        string token(pch);
        tokens.push_back(token);
        pch = strtok(NULL, ",");
    }
    delete [] str;
    return tokens;
}

int Elfove::stringToInt(string s) {
    stringstream ss(s);
    int tmp;
    if ((ss >> tmp).fail()) {
        throw "Neplatny token: " + s;
    }
    return tmp;
}

