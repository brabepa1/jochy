
#include <vector>

#include "Sprezeni.h"
#include "ClenSprezeni.h"
#include "UsporadanyKontejner.h"
#include "TaznaSila.h"
#include "Vuz.h"

using namespace std;

//tohle by se dalo vylepsi pouzivanim toho Vykonej ktery neumim pouzit
double Sprezeni::ZjistiVykon() const {
    double vykon = 0;
    for (vector<ClenSprezeni*>::const_iterator it = clenoveSprezeni.begin(); it != clenoveSprezeni.end(); ++it) {
        TaznaSila * ts = dynamic_cast<TaznaSila*> (*it);
        if (ts != NULL) {
            vykon += ts->getTaznyVykon();
        }
    }
    return vykon;
}

//tohle by se dalo vylepsi pouzivanim toho Vykonej ktery neumim pouzit
double Sprezeni::ZjistiVahu() const {
    double vaha = 0;
    for (vector<ClenSprezeni*>::const_iterator it = clenoveSprezeni.begin(); it != clenoveSprezeni.end(); ++it) {
        Vuz * vuz = dynamic_cast<Vuz*> (*it);
        if (vuz != NULL) {
            vaha += vuz->getVaha();
        }
    }
    return vaha;
}

void Sprezeni::Vypis() const {
    for (vector<ClenSprezeni*>::const_iterator it = clenoveSprezeni.begin(); it != clenoveSprezeni.end(); ++it) {
        (*it)->Vypis();
    }
}

void Sprezeni::OdstranNadvahu() {
    while (!JeLetuSchopny()) {
        ClenSprezeni * cs;
        for (vector<ClenSprezeni*>::const_iterator it = clenoveSprezeni.begin(); it != clenoveSprezeni.end(); ++it) {
            Naklad * n = dynamic_cast<Naklad*> (*it);
            if (n != NULL) {
                cs = n;
                break;
            }
        }
        vector<ClenSprezeni*> forRem;
        forRem.push_back(cs);
        clenoveSprezeni.Odeber(forRem.begin());
    }
}

bool Sprezeni::JeLetuSchopny() const {
    return ZjistiVykon() >= ZjistiVahu();
}
