#ifndef USPORADANYKONTEJNER_H
#define	USPORADANYKONTEJNER_H

#include <vector>

#include "ClenSprezeni.h"

template<typename T, typename Comparer = std::less<T> >
class UsporadanyKontejner {
    
    typedef typename std::vector<T>::const_reverse_iterator reverse_iterator;
    typedef typename std::vector<T>::const_iterator iterator;

    Comparer cmp;
    std::vector<T> data;

public:

    UsporadanyKontejner();

    void Vloz(T prvek);

    void Odeber(iterator it);

    T& operator[](size_t index);
    const T& operator[](size_t index) const;

    template<typename K>
    void Vykonej(void(*ukazatel)(K&)) {
        for (unsigned int i = 0; i < data.size(); i++) {
            if (dynamic_cast<K&> (data[i]) != NULL) {
                ukazatel(data[i]);
            }
        }
    }

    template<typename K>
    K& DejPrvni(size_t& index) {
        for (unsigned int i = 0; i < data.size(); i++) {
            if (dynamic_cast<K&> (data[i]) != NULL) {
                index = i;
                return data[i];
            }
        }
    }

    template<typename K>
    K& DejPosledni(size_t& index) {
        for (unsigned int i = data.size() -1 ; i >= 0 ; i--) {
            if (dynamic_cast<K&> (data[i]) != NULL) {
                index = i;
                return data[i];
            }
        }
    }

    iterator begin() const{
        return data.begin();
    }
    iterator end() const{
        return data.end();
    }
    reverse_iterator rbegin() const {
        return data.rbegin();
    }
    reverse_iterator rend() const{
        return  data.rend();
    }

};

#endif	/* USPORADANYKONTEJNER_H */

